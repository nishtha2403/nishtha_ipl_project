const path = require('path');
const csv = require('csvtojson');

const  matchesPerYear = require('./problems/matchesPerYear');
const  matchesWonPerTeamPerYear = require('./problems/matchesWonPerTeamPerYear');
const  extraRunsPerTeamSpecificSeason = require('./problems/extraRunsPerTeamSpecificSeason');
const  topTenEconomicalBowlersSpecificSeason = require('./problems/topTenEconomicalBowlersSpecificSeason');
const  teamWonTossAndMatch = require('./problems/teamWonTossAndMatch');
const  highestPlayerOfMatchAwardEachSeason = require('./problems/highestPlayerOfMatchAwardEachSeason');
const  strikeRateOfBatsmanEachSeason = require('./problems/strikeRateOfBatsmanEachSeason');
const  highestTimesPlayerDismissed = require('./problems/highestTimesPlayerDismissed');
const  bowlerBestEconomySuperOver = require('./problems/bowlerBestEconomySuperOver');
const  convertDataToJson = require('./helpers/convertDataToJson');
const  createOutputFile = require('./helpers/createOutputFile');

/** 
 * Path of deliveries.csv file
 * @type {string} 
 */
const csvDeliveryPath = path.join(__dirname,'..','data','deliveries.csv');
/** 
 * Path of matches.csv file
 * @type {string} 
 */
const csvMatchPath = path.join(__dirname,'..','data','matches.csv');


/** 
 * Storing the returned deliveries promise object
 * @type {object}
 */
const deliveriesPromise = csv().fromFile(csvDeliveryPath)
/** 
 * Storing the returned matches promise object
 * @type {object}
 */
const matchesPromise = csv().fromFile(csvMatchPath)

Promise.all([deliveriesPromise,matchesPromise]).then((respArray) => {
    const deliveries = respArray[0];
    const  matches = respArray[1];

    matchesPerYearResult(matches);
    matchesWonPerTeamPerYearResult(matches);
    extraRunsPerTeamSpecificSeasonResult(matches,deliveries);
    topTenEconomicalBowlersSpecificSeasonResult(matches,deliveries);
    teamWonTossAndMatchResult(matches);
    highestPlayerOfMatchAwardEachSeasonResult(matches);
    strikeRateOfBatsmanEachSeasonResult(matches,deliveries);
    highestTimesPlayerDismissedResult(deliveries);
    bowlerBestEconomySuperOverResult(deliveries);


}).catch(console.error);

/**
 * Calls the required functions to get the final output file for the
 * number of matches played per year for all the years in IPL
 * @param {Array<object>} matches Array of matches object
 */
let matchesPerYearResult = (matches) => {
    const result = matchesPerYear(matches);
    const resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'matchesPerYear');
};

/**
 * Calls the required functions to get the final output file for the
 * number of matches won per team per year in IPL
 * @param {Array<object>} matches Array of matches object
 */
let matchesWonPerTeamPerYearResult = (matches) => {
    const result = matchesWonPerTeamPerYear(matches);
    const resultInJsonStringified  = convertDataToJson(result); 
    createOutputFile(resultInJsonStringified ,'matchesWonPerTeamPerYear');
}

/**
 * Calls the required functions to get the final output file for the
 * extra runs conceded per team in the year 2016
 * @param {Array<object>} matches Array of matches object
 */
let extraRunsPerTeamSpecificSeasonResult= (matches,deliveries) => {
    const result = extraRunsPerTeamSpecificSeason(matches,deliveries,2016);
    const resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'extraRunsPerTeamSpecificSeason');
}

/**
 * Calls the required functions to get the final output file for the
 * top 10 economical bowlers in the year 2015
 * @param {Array<object>} matches Array of matches object
 */
let topTenEconomicalBowlersSpecificSeasonResult = (matches,deliveries) => {
    const result = topTenEconomicalBowlersSpecificSeason(matches,deliveries,2015);
    const resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'topTenEconomicalBowlersSpecificSeason');
}

/**
 * Calls the required functions to get the final output file for the
 * number of times each team won the toss and also won the match
 * @param {Array<object>} matches Array of matches object
 */
let teamWonTossAndMatchResult = (matches) => {
    const result = teamWonTossAndMatch(matches);
    const resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'teamWonTossAndMatch');
}

/**
 * Calls the required functions to get the final output file for the
 * player who has won the highest number of Player of the Match awards for each season
 * @param {Array<object>} matches Array of matches object
 */
let highestPlayerOfMatchAwardEachSeasonResult = (matches) => {
    const result =  highestPlayerOfMatchAwardEachSeason(matches);
    const resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'highestPlayerOfMatchAwardEachSeason');

}

/**
 * Calls the required functions to get the final output file for the
 * strike rate of a batsman for each season
 * @param {Array<object>} matches Array of matches object
 */
let strikeRateOfBatsmanEachSeasonResult = (matches,deliveries) => {
    const result = strikeRateOfBatsmanEachSeason(matches,deliveries);
    let resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'strikeRateOfBatsmanEachSeason');
}

/**
 * Calls the required functions to get the final output file for the
 * highest number of times one player has been dismissed by another player
 * @param {Array<object>} matches Array of matches object
 */
let highestTimesPlayerDismissedResult = (deliveries) => {
    const result = highestTimesPlayerDismissed(deliveries);
    const resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'highestTimesPlayerDismissed');
}

/**
 * Calls the required functions to get the final output file for the
 * bowler with the best economy in super overs
 * @param {Array<object>} matches Array of matches object
 */
let bowlerBestEconomySuperOverResult = (deliveries) => {
    const result = bowlerBestEconomySuperOver(deliveries);
    const resultInJsonStringified  = convertDataToJson(result);
    createOutputFile(resultInJsonStringified ,'bowlerBestEconomySuperOver');
}