/** @module convertDataToJson */
/**
 * Converts data into JSON
 * @param {object} data data to be converted in JSON
 * @returns {string} Converted JSON 
 */
let convertDataToJson = (data) => {
    const json = JSON.stringify(data);
    return json;
}

module.exports = convertDataToJson;