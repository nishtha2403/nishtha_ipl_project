/** 
 * Module will create a file with required data in output directory 
 * @module  createOutputFile 
 */
const fs = require('fs');
const path = require('path');

/**
 * Creates file in src/public/output directory
 * @param {string} output The data inside the file
 * @param {string} filename Name of the file
 * @returns {void} 
 */

let createOutputFile = (output,filename) => {
    fs.mkdir(path.join(__dirname,'..','..','public','output'),() => {
        fs.writeFile(path.join(__dirname,'..','..','public','output',`${filename}.json`),output,'utf-8',() => {});
    });  
}

module.exports = createOutputFile;