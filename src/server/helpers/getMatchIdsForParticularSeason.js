/** 
 * Module will give the match ids for the required season
 * @module getMatchIdsForParticularSeason 
 */
/**
 * To get match ids for a particular season
 * @param {Array<object>} matchesObjArr Array of matches object
 * @param {string|number} season Season for which match Ids are required
 * @returns {Array<string>} Array of match ids for required season
 */
let getMatchIdsForParticularSeason = (matchesObjArr,season) => {

    let matchIdsOfParticularSeason = [];

    for(let matchesObj of matchesObjArr){
        if(matchesObj.season == season){
            matchIdsOfParticularSeason.push(matchesObj.id);
        }
    }

    return matchIdsOfParticularSeason;
    
}

module.exports = getMatchIdsForParticularSeason;