/** 
 * Module to find the highest number of times one player has been dismissed by another player
 * @module  highestTimesPlayerDismissed 
 */
/**
 * To calculate highest number of times one player has been dismissed by another player
 * @param {Array<object>} deliveries Array of delivery object
 * @returns {Array<object>} Array will store object in format {"player":"MS Dhoni","playerWhoDismissed":"Z Khan","numberOfTimes":7}
 */

let highestTimesPlayerDismissed = (deliveries) => {

    let playerGotDismissedBy = deliveries.reduce((acc,delivery) => {
        let player_dismissed = delivery['player_dismissed'];
        let dismissal_kind = delivery['dismissal_kind'];
        let playerWhoDismissed = delivery['bowler'];

        if(player_dismissed != '' && dismissal_kind != ''){
            if(dismissal_kind != 'run out'){

                if(acc.hasOwnProperty(player_dismissed)){

                    let playerDismissedObj = acc[player_dismissed];

                    if(playerDismissedObj.hasOwnProperty(playerWhoDismissed)){
                        playerDismissedObj[playerWhoDismissed] += 1;
                    }else{
                        playerDismissedObj[playerWhoDismissed] = 1;
                    }
                }else {
                    acc[player_dismissed] = {};
                    acc[player_dismissed][playerWhoDismissed] = 1;
                }
            }
        }

        return acc;
    },{});

    let maxTimesArray = [];
    Object.keys(playerGotDismissedBy).map((player) => playerGotDismissedBy[player]).forEach((playerObj) => {
        maxTimesArray.push(...Object.keys(playerObj).map((playerWhoDismissed) => playerObj[playerWhoDismissed]));
    });

    let maxTimes = Math.max(...maxTimesArray);

    let result = [];

    Object.keys(playerGotDismissedBy).reduce((acc,player) => {
        acc.push({
            player,
            playerObj:playerGotDismissedBy[player]
        })
        return acc;
    },[]).forEach((obj) => {
        let player = obj.player;
        let playerKeys = Object.keys(obj.playerObj).filter((playerWhoDismissed) => obj.playerObj[playerWhoDismissed] == maxTimes);
        if(playerKeys.length > 0){
            let playerWhoDismissed = playerKeys[0];
            let numberOfTimes = obj.playerObj[playerWhoDismissed];
             
            result.push({
                player,
                playerWhoDismissed,
                numberOfTimes
            });
        }
    });
    
    return result;
}

module.exports = highestTimesPlayerDismissed;