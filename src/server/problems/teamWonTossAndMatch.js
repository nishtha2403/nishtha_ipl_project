/** 
 * Module to find number of times each team won the toss and the match
 * @module teamWonTossAndMatch 
 */

/**
 * To calculate the number of times each team won the toss and also won the match
 * @param {Array<object>} matches Array of matches object 
 * @returns {Array<object>} Array will store objects in format {"team":"Sunrisers Hyderabad","wonTossAndMatch":17}
 */
let teamWonTossAndMatch = (matches) => {
    let teamWinObj = matches.reduce((acc,match) => {
        let winingTeam = match['winner'];
        let tossWinner = match['toss_winner'];
        if(winingTeam != ''){
            if(acc.hasOwnProperty(winingTeam)){
                if(winingTeam === tossWinner){
                    acc[winingTeam] += 1;
                }
            }else{
                acc[winingTeam] = 0;
                if(winingTeam === tossWinner){
                    acc[winingTeam] += 1;
                }
            }
        }

        return acc;
    },{});

    let result = Object.keys(teamWinObj).map((team) => {
        return {team: team, wonTossAndMatch: teamWinObj[team]}
    });

    return result;
}

module.exports = teamWonTossAndMatch;