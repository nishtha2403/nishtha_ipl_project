/** 
 * Module will give the top 10 economical bowlers for the required season
 * @module  topTenEconomicalBowlersSpecificSeason 
 */

/**
 * Module getMatchIdsForParticularSeason will give the match ids for the required season
 * {@link module:getMatchIdsForParticularSeason}
 */

const getMatchIdsForParticularSeason = require('../helpers/getMatchIdsForParticularSeason');

/**
 * To calculate top ten economical bowler of a particular season
 * @param {Array<object>} matches Array of matches object
 * @param {Array<object>} deliveries Array of delivery object
 * @param {string|number} season Season for which top ten economical bowler needs to be calculated
 * @returns {Array<object>} Array will store objects in format {bowler: 'J Yadav', economyRate: 4.41} 
 */

let topTenEconomicalBowlersSpecificSeason = (matches,deliveries,season) => {
    
    const matchIdsOfGivenSeason = getMatchIdsForParticularSeason(matches,season);

    const deliveriesForGivenSeason = matchIdsOfGivenSeason.map((matchId) => {
        return deliveries.filter((delivery) => matchId == delivery['match_id']);
    }).flat();

    // console.log(deliveriesForGivenSeason);
    
    /* 
    bowlerDetails object will store bowlername(as key) and it's value will hold an object of 
    totalRuns and totalBalls.

    bowlerDetails = {'KC Cariappa': { totalRuns: 28, totalBalls: 12 }, ...}
    */
    let bowlerDetails = deliveriesForGivenSeason.reduce((acc,delivery) => {
        let bowler = delivery.bowler;
        let totalRuns = parseInt(delivery['total_runs']);

        if(acc.hasOwnProperty(bowler)){
            let bowlerObj = acc[bowler];
            bowlerObj.totalRuns += totalRuns;
            bowlerObj.totalBalls += 1;
        }else {
            acc[bowler] = {totalRuns,totalBalls: 1};
        }

        return acc;
    },{});

    // bowlerEconomyRateObj will store bowlername(as key) and it's value will be calculated economy rate
    let bowlerEconomyRateObj = Object.keys(bowlerDetails).reduce((acc,bowler) => {
        let bowlerObj = bowlerDetails[bowler];
        let totalRuns = bowlerObj.totalRuns;
        let totalBalls = bowlerObj.totalBalls;
        acc[bowler] = parseFloat((totalRuns / (totalBalls/6)).toFixed(4));
        return acc;
    },{});

    // topTenEconomyRateArr will store the top 10 economy rate
    let topTenEconomyRateArr = Object.values(bowlerEconomyRateObj);

    topTenEconomyRateArr =  topTenEconomyRateArr.sort((a,b) => a-b).slice(0,10);

    /* 
    topTenEconomicalBowler will store bowlers with economy rate which is present in topTenEconomyRateArr array.

    topTenEconomicalBowler array will store objects in format {bowler: 'J Yadav', economyrate: 4.41} 
    */

    let topTenEconomicalBowler = topTenEconomyRateArr.map((economyRate) => {
        return Object.keys(bowlerEconomyRateObj).map((bowler) => {
            if(bowlerEconomyRateObj[bowler] == economyRate){
                return {bowler,economyRate: bowlerEconomyRateObj[bowler]};
            }
        }).filter((obj) => obj !== undefined);
    }).flat();

    // Since there can be more bowlers with same economic rate therefore slicing top 10 bowler
    topTenEconomicalBowler = topTenEconomicalBowler.slice(0,10);

    return topTenEconomicalBowler;
    
}

module.exports = topTenEconomicalBowlersSpecificSeason;