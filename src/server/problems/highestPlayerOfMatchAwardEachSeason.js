/** 
 * Module tofind a player who has won the highest number of Player of the Match awards for each season
 * @module highestPlayerOfMatchAwardEachSeason 
 */
/**
 * To calculate a player who has won the highest number of Player of the Match awards for each season
 * @param {Array<object>} matches Array of matches object
 * @returns {Array<object>} Array will store object in format {"season":"2008","highestPlayerOfMatchAward":"SE Marsh"}
 */
let highestPlayerOfMatchAwardEachSeason = (matches) => {
    let playerOfMatchEachSeason = matches.reduce((acc,match) => {
        let season = match.season;
        let playerOfMatch = match['player_of_match']

        if(acc.hasOwnProperty(season)){
            let seasonObj = acc[season];
            if(seasonObj.hasOwnProperty(playerOfMatch)){
                seasonObj[playerOfMatch] += 1;
            }else {
                seasonObj[playerOfMatch] = 1;
            }
        }else{
            acc[season] = {};
            acc[season][playerOfMatch] = 1;
        }

        return acc;
    },{});

    let result = [];

    Object.keys(playerOfMatchEachSeason).forEach((season) => {
        let seasonObj = playerOfMatchEachSeason[season];

        let playerAndFrequencyArray = Object.keys(seasonObj).map((player) => [player,seasonObj[player]]);
        
        playerAndFrequencyArray.sort((a,b) => {
            if((b[1]-a[1]) < 0)
                return b[1]-a[1];
        });

        result.push({
            season: season,
            highestPlayerOfMatchAward: playerAndFrequencyArray[0][0]
        });
    });

    return result;
}

module.exports = highestPlayerOfMatchAwardEachSeason;