/** 
 * Module to find the bowler with best economy rate in super overs
 * @module bowlerWithBestEconomyInSuperOver
 */

/**
 * To calculate bowler with the best economy rate in super overs
 * @param {Array<object>} deliveries Array of delivery objects 
 * @returns {Array<object>} Array will store objects in format {bowler:"JJ Bumrah",economyRate:4.5}
 */
let bowlerWithBestEconomyInSuperOver = (deliveries) => {
    let deliveriesSuperOvers = deliveries.filter((delivery) => delivery['is_super_over'] > 0);
    
    let bowlerDetails = deliveriesSuperOvers.reduce((acc,delivery) => {
        let bowler = delivery['bowler'];
        let totalRuns = parseInt(delivery['total_runs']);

        if(acc.hasOwnProperty(bowler)){
            acc[bowler]['totalRuns'] += totalRuns;
            acc[bowler]['totalBalls'] += 1;
        }else {
            acc[bowler] = {};
            acc[bowler]['totalRuns'] = totalRuns;
            acc[bowler]['totalBalls'] = 1;
        }

        return acc;
    },{});

    let economyRateArr = [];
    let bowlerWithEconomyRate = Object.keys(bowlerDetails).reduce((acc,bowler) => {
        let bowlerObj   = bowlerDetails[bowler];
        let totalRuns   = bowlerObj['totalRuns'];
        let totalBalls  = bowlerObj['totalBalls'];
        let economyRate = parseFloat((totalRuns / (totalBalls/6)).toFixed(4));

        economyRateArr.push(economyRate);

        acc.push({
            bowler,
            economyRate
        });

        return acc;
    },[]);

    let minEconomyRate = Math.min(...economyRateArr);

    let result = bowlerWithEconomyRate.filter((bowler) => bowler.economyRate == minEconomyRate);

    return result;

}

module.exports = bowlerWithBestEconomyInSuperOver;