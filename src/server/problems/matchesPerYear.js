/** 
 * Module to find number of matches played per year for all the years in IPL
 * @module matchesPerYear
 */

/**
 * To calculate number of matches played per year for all the years in IPL
 * @param {Array<object>} matches Array of matches object
 * @returns {Array<object>} Array will store object in format {"season":"2008","matches":58}
 */

let  matchesPerYear = (matches) => {

    //frequency object to store season and the frequency of matches played in that season
    let frequency = matches.reduce((acc,match) => {
        if(acc.hasOwnProperty(match.season)){
            acc[match.season] += 1;
        } else {
            acc[match.season] = 1;
        }
        return acc;
    },{});

    //result array will store objects in the format {season: '2008', matches: 58}
    let result = [];

    for(let season in frequency){
        result.push({
            season: season,
            matches: frequency[season]
        });
    }

    return result;

}

module.exports = matchesPerYear;