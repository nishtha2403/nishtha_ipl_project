/** 
 * Module to find number of matches won per team per year in IPL
 * @module  matchesWonPerTeamPerYear 
 */

/**
 * To calculate number of matches won per team per year in IPL
 * @param {Array<object>} matches Array of matches object
 * @returns {Array<object>} Array will store object in format {"season":"2008","team":"Kolkata Knight Riders","matchesWon":6}
 */

let matchesWonPerTeamPerYear = (matches) => {

    /* 
    seasonData object will store season as key and it's value will be object of 
    teams(as key) and matches won by team (as value).

    seasonData = { '2008' : {'KKR': 6, 'MM': 7 , ...}, ...}
    
    */
    let seasonData = matches.reduce((acc,match) => {
        if(acc.hasOwnProperty(match.season)){
            if(acc[match.season].hasOwnProperty(match.winner)){
                acc[match.season][match.winner] += 1;
            }else{
                acc[match.season][match.winner] = 1;
            }
        }else {
            acc[match.season] = {
                [match.winner] : 1
            }
        }

        return acc;
    },{})

    //result array will store objects in format {season: 2008 , team: 'KKR' , matchesWon: 6}
    let result = [];

    for(let season in seasonData){
        for(let team in seasonData[season]){
            result.push({
                season: season,
                team: team,
                matchesWon: seasonData[season][team]
            })
        }
    }
    
    return result;
}

module.exports = matchesWonPerTeamPerYear;