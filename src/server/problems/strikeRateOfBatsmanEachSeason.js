/** 
 * Module to find the strike rate of a batsman for each season
 * @module strikeRateOfBatsmanEachSeason 
 * */
/**
 * To calculate the strike rate of a batsman for each season
 * @param {Array<object>} matches Array of matches object
 * @param {Array<object>} deliveries Array of delivery object
 * @returns {Array<object>} Array will store object in format {"season":"2008","batsman":"SC Ganguly","strike_rate":119.1358}
 */
let strikeRateOfBatsmanEachSeason = (matches,deliveries) => {
    let matchIdSeason = matches.reduce((acc,match) => {
        let matchId = match['id'];
        let season = match['season'];
        acc[matchId] = season;

        return acc;
    },[]);

    let batsmanDetailForEachSeason = deliveries.reduce((acc,delivery) => {
        let matchId = delivery['match_id'];
        let season = matchIdSeason[matchId];
        let batsman = delivery['batsman'];
        let totalRuns = parseInt(delivery['total_runs']);

        if(acc.hasOwnProperty(season)){
            let seasonObj = acc[season];
            if(seasonObj.hasOwnProperty(batsman)){
                let batsmanObj = seasonObj[batsman];
                batsmanObj['total_runs'] += totalRuns;
                batsmanObj['total_balls'] += 1;
            }else{
                seasonObj[batsman] = {
                    total_runs: totalRuns,
                    total_balls: 1
                }
            }
        }else {
            acc[season] = {};
            acc[season][batsman] = {
                total_runs : totalRuns,
                total_balls : 1
            }
        }

        return acc;
    },{});

    let strikeRateForEachSeason = [];

    Object.keys(batsmanDetailForEachSeason).forEach((season) => {
        let seasonObj = batsmanDetailForEachSeason[season];

        let strikeRateSpecificSeason = Object.keys(seasonObj).map((batsman) => {
            let batsmanObj = seasonObj[batsman];
            let totalRuns = batsmanObj['total_runs'];
            let totalBalls = batsmanObj['total_balls'];
            let strikeRate = parseFloat(((totalRuns/totalBalls)*100).toFixed(4));

            return {season:season,batsman:batsman,strikeRate:strikeRate};
        });
        
        strikeRateForEachSeason.push(...strikeRateSpecificSeason); 
    });

    return strikeRateForEachSeason;
}

module.exports = strikeRateOfBatsmanEachSeason;