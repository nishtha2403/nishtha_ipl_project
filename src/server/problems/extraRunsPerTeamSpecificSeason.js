/** 
 * Module will find extra runs conceded per team for the required season
 * @module  extraRunsPerTeamInAnySeason 
 */
/**
 * Module getMatchIdsForParticularSeason will provide match ids for the reuired season
 * {@link module:getMatchIdsForParticularSeason}
 */
const getMatchIdsForParticularSeason = require('../helpers/getMatchIdsForParticularSeason');

/**
 * To Calculate extra runs conceded per team in the reuired season
 * @param {Array<object>} matches Array of matches object
 * @param {Array<object>} deliveries Array of delivery object
 * @param {string|number} season Season for which extra runs conceded per team needs to be calculated
 * @returns {Array<object>}
 */

let extraRunsPerTeamSpecificSeason = (matches,deliveries,season) => {

    const matchIdsOfGivenSeason = getMatchIdsForParticularSeason(matches,season);
    const deliveriesForGivenSeason =  matchIdsOfGivenSeason.map((matchId) => {
        return deliveries.filter((delivery) => delivery['match_id'] == matchId);
    }).flat();

    //extraRunsPerTeamObj will store team(as key) and extraRuns(as value)
    let extraRunsPerTeamObj = deliveriesForGivenSeason.reduce((acc,delivery) => {
        let team = delivery['bowling_team'];
        let extraRuns = parseInt(delivery['extra_runs']);
        if(acc.hasOwnProperty(team)){
            acc[team] += extraRuns;
        }else {
            acc[team] = extraRuns;
        }

        return acc;
    },{});


    //result array will store objects in format {team: 'KKR',extraRuns: 45}
    let result = Object.keys(extraRunsPerTeamObj).map((team) => {
        return {team, 'extraRuns': extraRunsPerTeamObj[team]}
    });

    return result;
}

module.exports = extraRunsPerTeamSpecificSeason;